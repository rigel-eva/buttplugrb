# Buttplugrb - Shove Ruby up your ass!

Welp ... You have wandered into probably one of the silliest things I set up ... This is a gem to make your ruby app act as a client for a buttplug.io server ... 

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'buttplugrb'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install buttplugrb

## Usage

You can get client up and running like this:

```ruby
require "buttplugrb"
client=Buttplug::Client.new("wss://localhost:12345/buttplug")
``` 

You can view some additional examples in the Examples Folder!

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
